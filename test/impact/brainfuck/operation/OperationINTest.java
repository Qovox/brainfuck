package impact.brainfuck.operation;

import impact.brainfuck.exception.AsciiException;
import impact.brainfuck.exception.InputException;
import impact.brainfuck.memory.Memory;
import impact.brainfuck.operation.OperationIN;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import static org.junit.Assert.*;

/**
 * @author Guillaume Andre
 * @version 2.0
 *          Test for OperationIN
 */
public class OperationINTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Before
    public void setUp() throws IOException {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void doOperation() throws IOException, AsciiException, InputException {
        Memory m = new Memory();
        File input = folder.newFile();
        BufferedWriter bw = new BufferedWriter(new FileWriter(input));
        bw.write("a");
        bw.close();
        OperationIN oi = new OperationIN();
        OperationIN.setBr(new BufferedReader(new FileReader(input)));
        oi.doOperation(m, 0);
        assertEquals((-128 + 'a'), m.getMemory()[0]);
    }

    @Test
    public void InStandard() throws AsciiException, InputException {
        Memory m = new Memory();
        ByteArrayInputStream in = new ByteArrayInputStream(("ab\na\n").getBytes());
        ByteArrayOutputStream outBa = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(outBa);
        System.setIn(in);
        System.setOut(out);
        OperationIN oi = new OperationIN();
        OperationIN.setSc(new Scanner(System.in));
        oi.doOperation(m, 0);
        assertEquals((-128 + 'a'), m.getMemory()[0]);
        String outputCheck = new String(outBa.toByteArray(), StandardCharsets.UTF_8);
        assertEquals("Input 1:\n" + "Input must be one character.\n" + "Input 1:\n", outputCheck);
    }

}
