package impact.brainfuck.operation;

import impact.brainfuck.check.LoopChecker;
import impact.brainfuck.interpreter.Interpreter;
import impact.brainfuck.main.Metrics;
import impact.brainfuck.memory.Memory;
import impact.brainfuck.operation.Operation;
import impact.brainfuck.operation.OperationINCR;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Guillaume Andre, Elliot Kauffmann
 * @version 2.1
 *          Test de OperationJUMP et OperationBACK.
 *
 *          if this throws NullPointerException on all 3 tests, that's because you're using IntelliJ.
 *          Seriously though, it comes from a bug with System.out and System.in, and is an IDE bug.
 *          Don't worry too much about it.
 *          Running this test class alone should give correct results.
 */
public class OperationJUMPBACKTest {

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Before
    public void setUp() {
        System.setIn(System.in);
        System.setOut(System.out);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void loopTest() throws Exception {
        Memory m = new Memory();
        OperationINCR oi = new OperationINCR();
        for (int i = 0; i < 4; i++) {
            oi.doOperation(m, 0);
        }
        List<Operation> inst = Arrays.asList(Operation.JUMP, Operation.DECR, Operation.RIGHT, Operation.INCR, Operation.LEFT, Operation.BACK);
        Interpreter i = new Interpreter(m, inst, null, System.out, null, new LoopChecker().check(inst),new HashMap<>(), new Metrics());
        //NullPointerException ? see above.
        i.run();
        assertEquals("C1: 4\n", m.display());
    }

    @SuppressWarnings("unused")
    @Test //(expected = LoopException.class)
    public void noBack() throws Exception {
        Memory m = new Memory();
        List<Operation> inst = Collections.singletonList(Operation.JUMP);
        exit.expectSystemExitWithStatus(4);
        Interpreter i = new Interpreter(m, inst, null, System.out, null, new LoopChecker().check(inst),new HashMap<>(), new Metrics());
        //NullPointerException ? see above.
        //doesn't actually need to run since the check takes place during the constructor
    }

    @SuppressWarnings("unused")
    @Test //(expected = LoopException.class)
    public void noJump() throws Exception {
        Memory m = new Memory();
        OperationINCR oi = new OperationINCR();
        oi.doOperation(m, 0);
        List<Operation> inst = Arrays.asList(Operation.INCR, Operation.BACK);
        exit.expectSystemExitWithStatus(4);
        Interpreter i = new Interpreter(m, inst, null, System.out, null, new LoopChecker().check(inst),new HashMap<>(), new Metrics());
        //NullPointerException ? see above.
        //doesn't actually need to run since the check takes place during the constructor
    }
}
