package impact.brainfuck.generator;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import impact.brainfuck.operation.Operation;

/**
 * @author Elliot Kauffmann
 * 
 * This class is the JUnit test class for the Java code generator.
 * 
 * It tests whether the beginning and end of the generated code contains
 * specific Strings.
 * 
 * It also tests whether the code generator is capable of generating individual
 * instructions, and finally it tests whether it is capable of translating all
 * the instructions in the correct order.
 */
public class JavaGeneratorTest {

	/** Test file that holds the code to be tested */
	File javaGenTestFile;
	/** Code Generator */
	JavaGenerator javaGen;
	/** Instructions to be translated */
	List<Operation> list;

	/** Test file reader */
	FileReader fr;
	/** Reads the lines of the test file one after the other */
	BufferedReader br;

	/**
	 * Initializes a temporary file for the tests to write in, the instruction
	 * List and the code generator.
	 * 
	 * Also initializes a FileReader and a BufferedReader to read the file after
	 * we've written in it.
	 */
	@Before
	public void setUp() throws Exception {

		javaGenTestFile = new File("cGen.txt");
		javaGenTestFile.deleteOnExit();
		System.setOut(new PrintStream(javaGenTestFile));
		javaGen = new JavaGenerator();
		list = new ArrayList<>();

		fr = new FileReader(javaGenTestFile);
		br = new BufferedReader(fr);
	}


	/**
	 * Clean everything up after the test.
	 */
	@After
	public void tearDown() throws Exception {

		fr.close();
		br.close();
		
		javaGenTestFile.delete();
		System.out.close();
	}

	/**
	 * This tests whether the beginning and end of the generated code contains
	 * specific lines.
	 */
	@Test
	public void beginningAndEndTest() throws Exception {

		javaGen.generate(list, System.out);
		String expectedBeginningLines[] = { "import java.io.IOException;\n", "public class Main {\n",
				"public static void main(String[] args) throws IOException {\n", "int p = 0;\n", "byte[] memory = new byte[30000];\n" };

		String expectedEndLine = "}\n}\n";
		StringBuilder allLines = new StringBuilder();
		String line;

		// Read all the lines up to the code generation marker
		do {
			line = new String(br.readLine());
			allLines.append(line).append("\n");
		} while (!(line.contains("// Code generation from BrainF*ck to Java begins here")));

		line = allLines.toString();
		allLines = new StringBuilder();
		// Not using a for Loop, so we know which line is not being represented
		// in case of an error.
		assertTrue(line.contains(expectedBeginningLines[0]));
		assertTrue(line.contains(expectedBeginningLines[1]));
		assertTrue(line.contains(expectedBeginningLines[2]));
		assertTrue(line.contains(expectedBeginningLines[3]));
		assertTrue(line.contains(expectedBeginningLines[4]));

		// Read all the lines to the end of the file.
		do {
			line = br.readLine();
			allLines.append(line).append("\n");
		} while (line != null);

		line = allLines.toString();

		assertTrue(line.contains(expectedEndLine));
	}

	/**
	 * The next 8 methods test whether the code generator is capable of
	 * generating individual instructions.
	 */

	@Test
	public void incrTest() throws Exception {

		list.add(Operation.INCR);
		javaGen.generate(list, System.out);
		String expectedInstruction = "memory[p]++;";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to Java begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstruction));
	}

	@Test
	public void decrTest() throws Exception {

		list.add(Operation.DECR);
		javaGen.generate(list, System.out);
		String expectedInstruction = "memory[p]--;";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to Java begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstruction));
	}

	@Test
	public void jumpTest() throws Exception {

		list.add(Operation.JUMP);
		javaGen.generate(list, System.out);
		String expectedInstruction = "while (memory[p] != 0) {";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to Java begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstruction));
	}

	@Test
	public void backTest() throws Exception {

		list.add(Operation.BACK);
		javaGen.generate(list, System.out);
		String expectedInstruction = "}";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to Java begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstruction));
	}

	@Test
	public void leftTest() throws Exception {

		list.add(Operation.LEFT);
		javaGen.generate(list, System.out);
		String expectedInstruction = "p--;";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to Java begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstruction));
	}

	@Test
	public void rightTest() throws Exception {

		list.add(Operation.RIGHT);
		javaGen.generate(list, System.out);
		String expectedInstruction = "p++;";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to Java begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstruction));
	}

	@Test
	public void inTest() throws Exception {

		list.add(Operation.IN);
		javaGen.generate(list, System.out);
		String expectedInstruction = "System.in.read(memory, p, 1);";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to Java begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstruction));
	}

	@Test
	public void outTest() throws Exception {

		list.add(Operation.OUT);
		javaGen.generate(list, System.out);
		String expectedInstruction = "System.out.write(memory[p]);";

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to Java begins here"))
			;
		assertTrue(br.readLine().contains(expectedInstruction));
	}

	/**
	 * This tests whether the code generator is capable of translating all the
	 * instructions in the correct order.
	 */
	@Test
	public void allInstructionsTest() throws Exception {

		list.add(Operation.INCR);
		list.add(Operation.DECR);
		list.add(Operation.LEFT);
		list.add(Operation.RIGHT);
		list.add(Operation.IN);
		list.add(Operation.OUT);
		list.add(Operation.JUMP);
		list.add(Operation.BACK);
		javaGen.generate(list, System.out);
		String expectedInstructions[] = { "memory[p]++;", "memory[p]--;", "p--;", "p++;", "System.in.read(memory, p, 1);",
				"System.out.write(memory[p]);", "while (memory[p] != 0) {", "}" };

		// Find the line that begins code generation.
		while (!br.readLine().contains("// Code generation from BrainF*ck to Java begins here"))
			;
		// Not using a for Loop, so we know which line is not being represented
		// in case of an error.
		assertTrue(br.readLine().contains(expectedInstructions[0]));
		assertTrue(br.readLine().contains(expectedInstructions[1]));
		assertTrue(br.readLine().contains(expectedInstructions[2]));
		assertTrue(br.readLine().contains(expectedInstructions[3]));
		assertTrue(br.readLine().contains(expectedInstructions[4]));
		assertTrue(br.readLine().contains(expectedInstructions[5]));
		assertTrue(br.readLine().contains(expectedInstructions[6]));
		assertTrue(br.readLine().contains(expectedInstructions[7]));
	}
}