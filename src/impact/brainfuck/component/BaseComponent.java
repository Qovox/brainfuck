package impact.brainfuck.component;

import impact.brainfuck.macro.FunctionCall;
import impact.brainfuck.operation.Operation;

import java.util.List;
import java.util.Map;

/**
 * @author Guillaume André
 *         <p>
 *         Represents a block of simple brainfuck code in an element (function or macro).
 *         For example, the macro a? +++ / contains the simple component "+++".
 */
public class BaseComponent implements Component {

    private List<Operation> operations;
    private List<Integer> parameters;
    private List<String> inputs;

    /**
     * Standard constructor.
     *
     * @param operations - the list of operations of this block
     */
    public BaseComponent(List<Operation> operations) {
        this.operations = operations;
    }

    /**
     * Translates this component into its corresponding brainfuck operations.
     * Stores the parameters and inputs given to give them back later.
     *
     * @param params           - the parameters acquired in brainfuck code
     * @param instructionsSize - the number of instructions read before - used to properly position function calls
     * @param functions        - the map where functions calls should be inserted
     * @param inputs           - the inputs acquired in brainfuck code
     * @return the list of instructions of this component
     */
    @Override
    public List<Operation> componentToList(List<Integer> params, int instructionsSize, Map<Integer, List<FunctionCall>> functions, List<String> inputs) {
        parameters = params;
        this.inputs = inputs;
        return operations;
    }

    /**
     * Gives the updated parameters list after componentToList() was used.
     * Base components never needs parameters, so this is always the same list as the one given in
     * the componentToList() method.
     * @return the parameters list after the required parameters were extracted
     */
    @Override
    public List<Integer> getParametersAfterToList() {
        return parameters;
    }

    /**
     * Gives the updated inputs list after componentToList() was used.
     * Base components never needs inputs, so this is always the same list as the one given in
     * the componentToList() method.
     * @return the inputs list after the required inputs were extracted
     */
    @Override
    public List<String> getInputsAfterToList() {
        return inputs;
    }

    @Override
    public String toString() {
        return operations.toString();
    }
}
