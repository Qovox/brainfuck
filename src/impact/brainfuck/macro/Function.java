package impact.brainfuck.macro;

import impact.brainfuck.component.Component;
import impact.brainfuck.exception.BadArgumentsException;
import impact.brainfuck.exception.SyntaxErrorException;
import impact.brainfuck.operation.Operation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Guillaume Andre
 *         <p>
 *         A brainfuck function. Extends a Macro, since most of their behavior is similar.
 */
public class Function extends Macro {

    public static boolean allowFunctions = true;

    private boolean allowReturn;
    private boolean defaultInput;

    /**
     * Standard constructor.
     *
     * @param df        - the list of all defined elements
     * @param line      - the line containing the function definition
     * @param functions - the map of the function calls
     * @throws SyntaxErrorException  - if the definition of the macro is incorrect
     * @throws BadArgumentsException - if trying to use functions with one of the incompatible arguments
     */
    public Function(DefinedElements df, String line, Map<Integer, List<FunctionCall>> functions) throws SyntaxErrorException, BadArgumentsException {
        super(df, line, functions);
        if (!allowFunctions) {
            throw new BadArgumentsException("Options --translate, --rewrite, --genc, --genjava and --check" +
                    "are not compatible with brainfuck functions.");
        }
    }


    /**
     * Treats function specific tokens, and sets the regex used for name recognition.
     *
     * @param line - the line containing this function definition
     * @return the same line, without the specific tokens and the name of the function
     * @throws SyntaxErrorException - if the name of this function was not valid
     */
    @Override
    public String setRegexAndRemoveExtras(String line) throws SyntaxErrorException {
        regex = "^[a-z0-9_]+!";
        allowReturn = line.contains("$");
        line = line.replaceFirst("\\$", "");
        line = extractName(line);
        if (line.startsWith("\"")) {
            defaultInput = true;
            inputCount++;
            line = line.substring(1);
        }
        definedElements.setRecursive(this);
        return line;
    }

    /**
     * Checks if this element must return a value or not.
     *
     * @return true if this element allows a return value, false otherwise
     */
    public boolean isAllowReturn() {
        return allowReturn;
    }

    /**
     * Checks if this function uses a custom input for the IN operations in its code.
     * This input will not be used for the custom inputs of the functions contained within this function.
     * If true, this input will have to be specified when this function is used anywhere in the brainfuck code.
     * This input will precede all other inputs if this function uses multiple inputs.
     *
     * @return true if a custom input is used, false otherwise
     */
    @Override
    public boolean isDefaultInput() {
        return defaultInput;
    }

    /**
     * Translates this function into its corresponding instructions, or adds it as a functionCall in the function map.
     * If ExecTime is set to true, it will prevent recursive functions from expanding until stack overflows.
     *
     * @param parameters      - the parameters of the macro/function, acquired in the brainfuck code
     * @param inputs          - this function custom input
     * @param instructionSize - the number of instructions parsed before this macro/function
     * @param nestedFunctions - the map where function calls should be added
     * @param atExecTime      - whether this call was made within an interpreter or not
     * @return the corresponding list of instructions, or an empty list if a new functionCall was set up
     * @throws SyntaxErrorException - if an incorrect number of parameters or inputs was used
     */
    @Override
    public List<Operation> toList(List<Integer> parameters, List<String> inputs, int instructionSize,
                                  Map<Integer, List<FunctionCall>> nestedFunctions, boolean atExecTime) throws SyntaxErrorException {
        if (!atExecTime) {
            nestedFunctions.putIfAbsent(instructionSize, new ArrayList<>());
            nestedFunctions.get(instructionSize).add(new FunctionCall(inputs, this, parameters));
            return new ArrayList<>();
        }
        List<Operation> result = new ArrayList<>();
        for (Component component : components) {
            result.addAll(component.componentToList(parameters, instructionSize + result.size(), nestedFunctions, inputs));
            parameters = component.getParametersAfterToList();
        }
        return result;
    }

}
