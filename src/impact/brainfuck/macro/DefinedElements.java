package impact.brainfuck.macro;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Guillaume André
 *         <p>
 *         Stores a list of all defined elements (macros and functions).
 */
public class DefinedElements {

    private List<Macro> definedMacros;
    private Function recursive;

    public DefinedElements() {
        definedMacros = new ArrayList<>();
    }

    /**
     * Searches for a known macro in the list.
     *
     * @param search - the name of the macro
     * @return the macro if found, false otherwise
     */
    public Macro getMacro(String search) {
        if (recursive != null && recursive.name.equals(search)) {
            return recursive;
        }
        for (Macro m : definedMacros) {
            if (m.name.equals(search)) {
                return m;
            }
        }
        return null;
    }

    /**
     * Adds a macro to the list of known macros.
     *
     * @param m - the new macro to be added
     */
    public void add(Macro m) {
        definedMacros.add(m);
    }

    /**
     * Allows a function not completely defined to be used in a component.
     * This makes recursive functions possible.
     *
     * @param recursive - the function only partially defined
     */
    public void setRecursive(Function recursive) {
        this.recursive = recursive;
    }

    public String toString() {
        return definedMacros.toString();
    }
}
