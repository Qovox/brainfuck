package impact.brainfuck.exception;

/**
 * Thrown when something went wrong with the output file.
 * 
 * @author Guillaume Andre
 * @version 2.0
 */
public class OutputException extends Exception {

	private static final long serialVersionUID = 6505015852927403719L;

	public OutputException() {
		super("Was unable to write to output file.");
		System.out.println(this.getMessage());
		System.exit(8);
	}

}
