package impact.brainfuck.exception;

/**
 * Exception thrown when the interpreter fails to read the brainfuck program file.
 *
 * @author Guillaume André
 * @version 3.0
 */
public class ReadException extends Exception {

	private static final long serialVersionUID = 7794767466892622404L;

	public ReadException() {
        super("Could not read brainfuck program file.");
        System.out.println(this.getMessage());
        System.exit(7);
    }

}
