package impact.brainfuck.check;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import impact.brainfuck.exception.LoopException;
import impact.brainfuck.operation.Operation;

/**
 * @author Guillaume Andre, Elliot Kauffmann
 * @version 3.0
 * 
 * This class checks the program for errors in JUMP/BACK loops. Should always be
 * used before execution.
 */
public class LoopChecker {

	/**
	 * Checks a given instructions list for errors in JUMP/BACK loops.
	 *
	 * @param instructions : the instructions list to check
	 * @throws LoopException if a loop error was found
	 */
	public Map<Integer, Integer> check(List<Operation> instructions) throws LoopException {
		Map<Integer, Integer> jumpTable = new HashMap<>();
		List<Integer> lastJumpIndex = new ArrayList<>();
		for (int i = 0; i < instructions.size(); i++) {
			if (instructions.get(i).equals(Operation.BACK)) {
				if (lastJumpIndex.isEmpty()) {
					throw new LoopException("BACK");
				} else {
					jumpTable.put(lastJumpIndex.get(lastJumpIndex.size() - 1), i);
					jumpTable.put(i, lastJumpIndex.remove(lastJumpIndex.size() - 1));
				}
			} else if (instructions.get(i).equals(Operation.JUMP)) {
				lastJumpIndex.add(i);
			}
		}
		if (!lastJumpIndex.isEmpty()) {
			throw new LoopException("JUMP");
		}
		return jumpTable;
	}
}