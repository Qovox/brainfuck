package impact.brainfuck.operation;

import impact.brainfuck.exception.LoopException;
import impact.brainfuck.memory.Memory;

import java.util.Map;

/**
 * @author Guillaume Andre, Elliot Kauffmann
 * @version 2.1
 *          Implementation of CommandOperation.
 *          Jumps to the associated back if current cell value is 0.
 *          Proceeds to the next instruction otherwise.
 */
public class OperationJUMP implements CommandOperation {

    private static Map<Integer, Integer> jumpTable;

    public static Map<Integer, Integer> getJumpTable() {return jumpTable; }

    public static void setJumpTable(Map<Integer, Integer> jumpTable) { OperationJUMP.jumpTable = jumpTable; }

    /**
     * Does nothing if current memory cell is not at 0.
     * Returns where the instruction pointer should move otherwise.
     *
     * @param memory                  - the memory in its current state
     * @param currentInstructionIndex - the instruction pointer
     * @return the position the instruction pointer should move to
     * @throws LoopException - if the associated BACK was not found
     */
    public int doOperation(Memory memory, int currentInstructionIndex) throws LoopException {
        if (memory.getMemory()[memory.getCurrentCellIndex()] != -128) {
            return currentInstructionIndex + 1;
        } else {
            return jumpTable.get(currentInstructionIndex) + 1;
        }
    }
}
