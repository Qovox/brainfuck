package impact.brainfuck.main;

public class Main {
    public static void main(String[] args) throws Exception {
        Launcher launcher = new Launcher(args);
        launcher.execute();
    }
}
