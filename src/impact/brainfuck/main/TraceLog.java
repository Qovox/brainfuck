package impact.brainfuck.main;

import impact.brainfuck.exception.OutputException;
import impact.brainfuck.interpreter.Interpreter;
import impact.brainfuck.memory.Memory;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * @author Florian Bourniquel
 * this class contains the bufferedWriter and its functions
 * to write a tracelog into a file
 */
public class TraceLog {

    private BufferedWriter bwLog;
    private Interpreter interpreter;
    private Memory memory;
    private long cptInstruction = 1;

    public TraceLog(BufferedWriter bwLog, Interpreter interpreter, Memory memory) {
        this.bwLog = bwLog;
        this.interpreter = interpreter;
        this.memory = memory;
    }

    /**
     * If log equals true, at each instruction, write in a text file relevant
     * information
     * @throws OutputException if we can't write in the file
     **/
    public void traceLog() throws OutputException {
        if (bwLog != null) {
            try {
                bwLog.write(new StringBuilder().append("Execution step number: ").append(cptInstruction).append('\n')
                        .append("Execution pointer location: ").append(interpreter.getCurrentInstructionIndex() + 1).append('\n').append(memory.display())
                        .append("\n\n").toString());
            } catch (IOException e) {
                throw new OutputException();
            }
        }
        cptInstruction++;
    }

    /**
     * Close the bufferedWriter
     * @throws OutputException if we can't close the bufferedWriter
     **/
    public void closeTraceLog() throws OutputException {
        if (bwLog != null) {
            try {
                bwLog.close();
            } catch (IOException e) {
                throw new OutputException();
            }
        }
    }

}
