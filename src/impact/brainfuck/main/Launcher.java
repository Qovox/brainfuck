package impact.brainfuck.main;

import impact.brainfuck.check.LoopChecker;
import impact.brainfuck.exception.BadArgumentsException;
import impact.brainfuck.generator.CGenerator;
import impact.brainfuck.generator.JavaGenerator;
import impact.brainfuck.interpreter.Interpreter;
import impact.brainfuck.memory.Memory;
import impact.brainfuck.operation.Operation;
import impact.brainfuck.reader.ImageReader;
import impact.brainfuck.reader.Reader;
import impact.brainfuck.reader.TextReader;
import impact.brainfuck.translator.InstrToImageTranslator;
import impact.brainfuck.translator.ShortTranslator;

import java.util.List;
import java.util.Map;

/**
 * @author Elliot Kauffmann, Florian Bourniquel, Guillaume Andre, Jeremy Lara
 * @version 2.1
 *          <p>
 *          This will execute when the user runs the program.
 *          Supported arguments:
 *          -p [brainfuck program path]  - Mandatory, indicates location of brainfuck program
 *          -i [input file path]         - Uses indicated file as input instead of standard input
 *          -o [output file path]        - Writes output to specified file instead of standard output
 *          -m                           - Enable metrics
 *          Supported options:
 *          --rewrite                    - Converts program to short syntax and writes result in output
 *          --translate                  - Translates a -bf program into its corresponding bitmap file
 *          --check                      - Checks program for JUMP/BACK mistakes
 *          --trace                      - Enable tracelog
 *          --genc                       - Generate c code
 *          --genjava                    - Generate java code
 *          System exits:
 *          0 - Everything went right
 *          1 - Memory cell overflow
 *          2 - Memory pointer out of bounds
 *          3 - One of the specified files was not found
 *          4 - Program contains incorrect JUMP/BACK instructions
 *          5 - Syntax error detected
 *          6 - Incorrect arguments used
 *          7 - Could not read brainf*ck program file
 *          8 - Could not write to output file
 *          9 - Could not process input file
 *          10 - Input file contains non ascii character
 */
public class Launcher {

    private String[] args;

    /**
     * Creates this interpreter launcher.
     * @param args the arguments used
     */
    public Launcher(String[] args) {
        this.args = args;
    }

    /**
     * Interprets a brainfuck program.
     * @throws Exception - if the interpretation could not be completed
     */
    public void execute() throws Exception {
        //Starting of the program execution
        long startTime = System.nanoTime();

        ArgumentManager am = new ArgumentManager(args);
        am.checkArguments();

        Reader r;
        if (am.getProgram().getName().endsWith(".bf")) {
            r = new TextReader(am.getProgram());
        } else if (am.getProgram().getName().endsWith((".bmp"))) {
            r = new ImageReader(am.getProgram());
        } else {
            throw new BadArgumentsException("only .bf text files and bitmaps are supported for interpretation.");
        }

        List<Operation> instructions = r.read();
        if (am.getOptions().isEmpty()) { //normal interpretation, no options
            Memory m = new Memory();
            LoopChecker lp = new LoopChecker();
            Map<Integer, Integer> jumpTable;
            jumpTable = lp.check(instructions);
            Metrics metrics = new Metrics();
            Interpreter i = new Interpreter(m, instructions, am.getInput(), am.getOutput(),am.getTracelog(), jumpTable, r.getFunctions(),metrics);
            i.run();
            System.out.print("\n" + m.display());
            Metrics.EXEC_TIME = System.nanoTime() - startTime;
            System.out.print(metrics.displayMetrics());
            System.exit(0);
        }

        for (String s : am.getOptions()) { //options => no interpretation
            switch (s) {
                case "--rewrite":
                    new ShortTranslator().translate(instructions,am.getOutput());
                    break;
                case "--translate":
                    new InstrToImageTranslator().translate(instructions, am.getOutput());
                    break;
                case "--genc":
                	new CGenerator().generate(instructions, am.getOutput());
                	break;
                case "--genjava":
                	new JavaGenerator().generate(instructions, am.getOutput());
                	break;
                case "--check":
                    LoopChecker lp = new LoopChecker();
                    lp.check(instructions);
                    break;
                default: //should never happen
                    throw new BadArgumentsException();
            }
        }
    }
}
