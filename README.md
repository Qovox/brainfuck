# IMPACT - Interprète BrainF*ck

## Auteurs

|||
|---------------|-----------------------------------|
|Guillaume André|guillaume.andre@etu.unice.fr		|
|Florian Bourniquel|florian.bourniquel@etu.unice.fr	|
|Jérémy Lara|jeremy.lara@etu.unice.fr				|
|Elliot Kauffmann|elliot.kauffmann@etu.unice.fr		|

## Introduction

Cet outil permet de lire des programmes BrainF*ck et des les exécuter pour obtenir leur résultat. Nous l'avons développé au cours des quatre derniers mois.

## Construction du logiciel

* Script *CreateBfck.sh* :

Le script *CreateBfck.sh* permet de construire l'archive *bfck.jar* de manière automatique.
Il compile avec la commande *javac* les fichiers *.java* du dossier source *src*, en fichiers *.class* et les place dans un répertoire *bin*. Puis il archive ce répertoire dans une archive *bfck.jar* avec la commande *jar* et le supprime.

* Script *bfck* :

Le script *bfck* permet d'invoquer l'archive *bfck.jar* avec la commande *./bfck*.
Sur windows, il faut utiliser la commande *java -jar bfck.jar* car il n'exécute pas les fichiers sans extension.

## Fonctionnalités

- Lecture de programmes en syntaxe longue, syntaxe courte ou sous forme d'image.
- Ré-écriture de programmes vers la syntaxe courte, ou sous forme d'image.

- Vérification de la syntaxe des boucles dans le programme (automatiquement avant l'exécution ou demandé par l'utilisateur).
- Affichage optionnel des métriques d'exécution, à la fin de l'exécution d'un programme.
- Génération optionnelle d'un fichier Log de trace pendant l'exécution, pour pouvoir retracer les opérations effectuées.

- Gestion des macros et macros à paramètres inclues dans le code BrainF*ck.
- Gestion des fonctions.

- Ré-écriture des programmes vers des équivalents en langage C ou Java.

## Paramètres

Arguments supportés:

*          -p [chemin programme brainfuck]  - Obligatoire, indique la location du programme brainfuck
*          -i [chemin du fichier input]     - Lire les entrées dans le fichier indiqué plutôt que sur l'entrée strandard
*          -o [chemin du fichier output]    - Ecrire les sorties dans le fichier indiqué plutôt que sur la sortie strandard
*          -m                               - Activer les metrics
 
Options supportées:

*          --rewrite                        - Traduire le programme bf en syntaxe courte
*          --translate                      - Traduire le programme bf en une image bitmap
*          --check                          - Verifier la présence d'erreurs dans les JUMP/BACK
*          --trace                          - Activer le tracelog
*          --genc                           - Generation du code c
*          --genjava                        - Generation du code java
 
## Tests

Pour nos tests unitaires nous avons utilisé une bibliothèque externe System Rules qui nous permet de tester les System.exit().
La version utilisée est celle présente à la racine du depot. 
Avant de lancer les tests il faut ajouter cette bibliothèque aux libraries du projet.